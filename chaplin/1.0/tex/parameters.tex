\label{sec:PARAMETERS}

A \chaplin database is a collection of results sampled by a set of
visualization parameters. Each parameter is described with an entry in
\parameterlist section of the json file, a key-value
pair of the internal name of the parameter and the JSON
dictionary that describes the parameter.

% Begin DeMarle: fill out these sections and un-comment
\begin{comment}
\subsubsubsection{Label (Required)}

\todo{DeMarle: describe}

\subsubsubsection{Values (Required)}

\todo{DeMarle: describe}

\subsubsubsection{Default Value (Required)}

\todo{DeMarle: describe}
\end{comment}
% End DeMarle: fill out these sections and un-comment

\subsubsubsection{Role (Optional)}

\todo{DeMarle: reference parameter examples based on role types}

Parameters may also have a \textit{role}. The roll more fully describes the
purpose of the parameter for generation and display purposes. A role of
``layer'' indicates that this parameter defines what specific object or objects
should be shown. A role of ``control'' indicates that this parameter is one of
a set that controls aspects of a specific object (i.e. filter settings). A role
of ``field'' indicates that this parameter represents a set of color inputs.

\subsubsubsection{Value Range (Optional)}

\todo{DeMarle: reference examples}

For parameters with a role of ``field'' the additional annotations
\textit{types} and \textit{valueRanges} give the specific image component type
corresponding to each possible value and the minimum and maximum numerical
values for image channels with the ``types'' designation of ``value'' image.

\subsubsubsection{Display Hint (Optional)}

Parameters may also have a suggested widget \textit{type}. The type is used as
a hint for viewing applications to inform what type of GUI widget is most
appropriate for this control. A type of ``hidden'' should generally not be
displayed to the user. A type of ``range'' is best displayed with a menu or
scalar bar that lets the user select one value at a time. A type of ``option''
is best displayed with a combobox type widget that lets the user select many
values at once.

% Begin DeMarle: fill out these sections and un-comment
\begin{comment}
\subsubsubsection{Channels (Optional)}

\todo{DeMarle: describe and reference output channel examples}
\end{comment}
% End DeMarle: fill out these sections and un-comment

\subsubsubsection{Parameter Example: Time}
\label{sec:Time}

Time varying data can be sampled at arbitrary points along the temporal domain.
\begin{verbatim}
 "parameter_list": {
   "time": { "default": "0.000000e+00",
             "values": ["0.000000e+00", "1.000737e-04", "1.999051e-04"],
             "type": "range",
             "label": "time" },
   ...
 }
\end{verbatim}

\subsubsubsection{Parameter Example: Camera Positions}
\label{sec:camerapositions}

In a traditional visualization setting the user can manipulate the camera arbitrarily. For cinema we discretize and organize the range of captured motions into of several camera motion classes. 
The camera model in use within a database is listed in the \cameramodel
entry of the json files metadata section. 
See section~\ref{sec:cameramodel} for specifics. 

\begin{itemize}
\item \camstatic No addition information needed in this section. 
Static cameras do not require a corresponding parameter entry.

\item \camphi A model in which the discrete camera positions are described by the product of two parameters with angular positions. Additional information needed defines the \cphi and \ctheta values. The camera ``from'' position varies over a set of regularly sampled angular positions centered around a chosen focal point. These cameras will have one or two corresponding parameters entries. In Cinema, \cphi is defined to be rotations around the vertical axis, going from -180 to 180 degrees, inclusive on the negative only. \ctheta is defined to be rotations from south to north pole, ranging from -90 to 90 degrees inclusive.
\begin{verbatim}
 "parameter_list": {
   "phi": { "default": -180,
            "values": [-180, -150, -120, -90, -60, -30,
                       0,
                       30, 60, 90, 120, 150],
            "type": "range",
            "label": "phi" },
   "theta": { "default": -90,
              "values": [-90, -64, -38, -12, 13, 38, 63, 88],
              "type": "range",
              "label": "theta" },
   ...
 }
\end{verbatim}
\item \camyaw and \camazimuth both require a \pose section. 
In these more general \pose based cameras, we store complete camera reference frames from an arbitrary collection of viewpoints. 

These cameras require a \pose parameter, which contains any number of 3x3 normalized camera reference frames. To keep the combination of camera positions over time and view directions manageable, we vary the camera's local coordinate frame consistently at every time step, but offset them from a different location at each time step. An example is shown below. 
\begin{verbatim}
 "parameter_list": {
  "time": {
    "values": ["0.0e+00",
               "1.0e-04",
               "8.0e-04"],
     ... },
  "pose": {
    "values": [[[-1.0, 1.224e-16, 7.498e-33],
                [0.0, 6.123e-17, -1.0],
                [-1.224e-16, -1.0, -6.123e-17]],
               [[-1.0, 1.109e-16, 5.175e-17],
                [0.0, 0.422, -0.906],
                [-1.224e-16, -0.906, -0.422]],
               ... ],
    ...
    },
  ...
 }
\end{verbatim}
\end{itemize}

\subsubsubsection{Parameter Example: Objects}
\label{sec:objects}

Different items may be displayed in the scene. A visibility parameter controls which items are visible at any given time. 
The viewer uses the same parameter to allow multiple items to be visible and depth composites multiple image channels together to produce the effect. 
Visibility parameters have a \textit{role} annotation of ``layer''.

\todo{DeMarle: is visibility included in the database? If so, where?}


\begin{verbatim}
 "parameter_list": {
   "vis": {
     "values": [
       "Contour1",
       "Wavelet1",
       ...
     ],
     "role": "layer",
     "type": "option",
     ...
   }
 }
\end{verbatim}

\subsubsubsection{Parameter Example: Operators}
\label{sec:operators}

Zero or more operators, such as clipping plane and isocontour samples along with their respective ranges are represented by parameters. Operators, like other parameters are cumulative. For each operator, the value is applied and the resulting scene is recorded. Operator parameters have a \textit{role} annotation of ``control''.

\begin{verbatim}
 "parameter_list": {
    "Contour1": { "values": [37.3531, 97.2221, 157.091, 216.96, 276.829],
                  "role": "control",
                  ... },
    ...
 }
\end{verbatim}

\subsubsubsection{Parameter Example: Output Image Channels}
\label{sec:image_channels}

In a Cinema workflow, the application producing a Cinema Database creates a set of image channels for each sample in the parameter space. Viewing applications use these channels to draw objects and to color them dynamically dependent on the user's choices for solid color or colormapped value arrays. Recall that ``types'' and ``valueRanges'' annotations are important for interpreting the output of each possible value for a color control.

The types entries describe the specific image channels type. There are several possible image channels.

\begin{itemize}
\item \textbf{Required}. One of the following image channels is required. Both \textbf{cannot} be present.
\begin{itemize}
\item \textbf{RBG}. An image containing colored pixel information. This can be one of two types:
\begin{itemize}
\item \textbf{Color} channel. This encodes a standard RGB value for each pixel - the result of rendering the viewpoint from the camera. 
Color images can not be dynamically color mapped.
\item \textbf{LUT (Lookup Table)} channel. This contains pixels that have been colored by applying a colormap to a single variable.
\end{itemize}
\item \textbf{Value} channel. This encodes an arbitrary array value associated with the data that is visualized at each pixel. Global ranges for each array, i.e. the min and max value for a value over all time steps and parameter settings, are recorded in the meta data file. In \chaplin, value images are stored as floating point image channels, where the value is either normalized between 0.0 and 1.0 or kept unmodified, and stored in a ``.Z'' file. This is described more fully in section~\ref{sec:valuemode}.
\end{itemize}
\item \textbf{Optional}. These channels enable other types of operations.
\begin{itemize}
\item \textbf{Depth} channel. This encodes a depth value for every pixel, relative to the camera. Required for compositing. In \chaplin, depth images are stored as floating point image channels, ranging from 0 for the near plane to 255 for the far plane, and kept in zlib compressed ``.Z'' files. As with value images the word size is 32bit. The endianness and image dimensions are recorded in the metadata section's endian and image\_size entries.
\item \textbf{Luminance} channel. This encodes a rendered shading brightness for each pixel. Required for lighting to be included in the final rendering. In \chaplin, luminance images are stored in RGB images where the R component contains the ambient gray value, G contains the diffuse, and B contains the specular component. Of these, currently only the diffuse component is used. All components range from 0 to 255.
\end{itemize}
\end{itemize}

An example of this parameter list section follows. Here we have all possibilities for the image channels of an object named ``Contour1''. The image channels consist of depth, luminance, and a value for a scalar quantity called ``RTData''. The RTData array varies over the entire simulation and for all objects between 37 and 277.

\begin{verbatim}
 "parameter_list": {
   "colorContour1": { "values": ["depth", "luminance", "RTData_0"],
                      "types": ["depth", "luminance", "value"],
                      "valueRanges": {"RTData_0": [37.3531, 276.829]},
                      "role": "field" },
   ...
  }
\end{verbatim}

